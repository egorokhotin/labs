package lab5.barBossHouse;

import java.util.Comparator;

//todo: ну ты выдумал имя конечно. Спрячь лабу и никому не показывай. Делай дальше шестую.
public class MenuItemDescendingComparator implements Comparator<MenuItem> {
    @Override
    public int compare(MenuItem it1, MenuItem it2) {
        int result = it1.compareTo(it2);
        if (result == -1) return 1;
        if (result == 1) return -1;
        else return 0;
    }
}
