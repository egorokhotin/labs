package lab5.barBossHouse;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class OrdersManagerIterator implements Iterator<Order> {
    private Order[] orders;
    private int index;

    public OrdersManagerIterator(OrdersManager manager) {
        orders = manager.getOrders();
        index = 0;
    }

    @Override
    public boolean hasNext() {
        return index + 1 < orders.length;
    }

    @Override
    public Order next() {
        if (hasNext()) {
            index++;
            return orders[index];
        } else throw new NoSuchElementException();
    }
}
