package lab5.barBossHouse;

import java.time.LocalDateTime;
import java.util.*;

public class TableOrder implements Order {
    private static final byte DEFAULT_ORDER_LENGTH = 50;
    private static final Customer DEFAULT_CUSTOMER_ORDER_VALUE = new Customer();
    private MenuItem[] menuItems;
    private LocalDateTime time;
    private byte size;
    private Customer customer;
    //private LinkedList<MenuItem> list;

    public TableOrder() {
        this(Util.DEFAULT_ORDER_LENGTH, Util.DEFAULT_CUSTOMER_ORDER_VALUE);
    }

    public TableOrder(byte dishCount, Customer customer) {
        if (dishCount < 0) throw new NegativeSizeException("Incorrect dishCount!");
        menuItems = new MenuItem[dishCount];
        size = getSize();
        this.customer = customer;
        this.time = LocalDateTime.now();
    }

    public TableOrder(MenuItem[] dishesArray, Customer customer) {
        menuItems = dishesArray;
        size = getSize();
        this.customer = customer;
        this.time = LocalDateTime.now();
        //list = new LinkedList<MenuItem>();
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public LocalDateTime getTime() {
        return this.time;
    }

    public boolean addItem(MenuItem menuItem) {
        if (size < menuItems.length) {
            checkLaw(menuItem);
            menuItems[size] = menuItem;
            size++;
            return true;

        } else {
            MenuItem[] newMenuItem = new MenuItem[size * 2];
            System.arraycopy(menuItems, 0, newMenuItem, 0, size);
            menuItems = newMenuItem;
            checkLaw(menuItem);
            menuItems[size] = menuItem;
            size++;
            return true;
        }
    }

    public boolean removeItem(String itemName) {
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], itemName)) {
                System.arraycopy(menuItems, i + 1, menuItems, i, size - i);
//                for (int j = i; j < size; j++) {
//                    menuItems[j] = menuItems[j + 1];
//                }
                size--;
            }
        }
        return true;
    }

    public boolean removeItem(MenuItem item) {
        for (int i = 0; i < size; i++) {
            if (menuItems[i].equals(item)) {
                System.arraycopy(menuItems, i + 1, menuItems, i, size - i);
//                for (int j = i; j < size; j++) {
//                    menuItems[j] = menuItems[j + 1];
//                }
                size--;
            }
        }
        return true;
    }

    public int removeAllItems(String itemName) {
        int deletedDishCount = 0, j = 0;
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], itemName)) {
                menuItems[i] = null;
                deletedDishCount++;
            }
        }
        for (int i = 0; i < size - 1; i++) {
            if (menuItems[i] == null) {
                menuItems[i] = menuItems[i + 1];
                menuItems[i + 1] = null;
            }
        }
        size -= deletedDishCount;
        return deletedDishCount;
    }

    public int removeAllItems(MenuItem item) {
        int deletedDishCount = 0, j = 0;
        for (int i = 0; i < size; i++) {
            if (menuItems[i].equals(item)) {
                menuItems[i] = null;
                deletedDishCount++;
            }
        }
        for (int i = 0; i < size - 1; i++) {
            if (menuItems[i] == null) {
                j = i;
                while ((menuItems[j] == null) & j < size) {
                    Util.shiftArray(menuItems, j);
                    j++;
                }
            }
        }
        size -= deletedDishCount;
        return deletedDishCount;
    }

    public int getItemsCount() {
        return size;
    }

    public MenuItem[] getItems() {
        MenuItem[] result = new MenuItem[size];
        System.arraycopy(menuItems, 0, result, 0, size);
        return result;
    }

    public double getSummaryCost() {
        double result = 0;
        for (int i = 0; i < size; i++) {
            result += menuItems[i].getPrice();
        }
        return result;
    }

    public int getItemSummaryCost(String name) {
        int result = 0;
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], name)) result += menuItems[i].getPrice();
        }
        return result;
    }

    public String[] getItemsNames() {
        if (size != 0) {
            String[] buffer = new String[size];
            boolean isAdded = false;
            int indx = 0;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < indx; j++) {
                    if (isEqualsNames(menuItems[i], buffer[j])) {
                        isAdded = true;
                        break;
                    }
                }
                if (isAdded) {
                    isAdded = false;
                    continue;
                } else {
                    buffer[indx] = menuItems[i].getName();
                    indx++;
                }
            }
            String[] names = new String[indx];
            System.arraycopy(names, 0, buffer, 0, buffer.length);
            return names;
        }
        return null;
    }

    public MenuItem[] getItemsSortedToLow() {
        if (size != 0) {
            MenuItem[] result = new MenuItem[size];
            System.arraycopy(menuItems, 0, result, 0, size);
            Arrays.sort(menuItems, new MenuItemDescendingComparator());
            //Util.quickSortItems(result, 0, size - 1);
            return result;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TableOrder: ");
        builder.append(size);
        builder.append("\n");

        //String tmpString = "TableOrder: " + size + "\n";
        for (int i = 0; i < size; i++) {
            builder.append(menuItems[i].toString());
        }
        return builder.toString();
    }

    @Override
    public int size() {
        return getSize();
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) return true;
        else return false;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) return false;
        else {
            return isExist((MenuItem) o);
        }
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return new OrderIterator(this);
    }

    @Override
    public Object[] toArray() {
        return getItems();
    }

    @Override
    public <MenuItem> MenuItem[] toArray(MenuItem[] a) {
        return null;
    }

    @Override
    public boolean add(MenuItem menuItem) {
        return addItem(menuItem);
    }

    @Override
    public boolean remove(Object o) {
        return remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        try {
            for (Object o : c) {
                if (!add((MenuItem) o)) {
                    return false;
                }
            }
        } catch (ClassCastException ex) {
            return false;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {

        Iterator<? extends MenuItem> it = c.iterator();
        MenuItem buf = it.next();
        int count = 0;
        for (int i = index; i < menuItems.length; i++) {
            menuItems[i] = buf;
            buf = it.next();
            count++;
            if (buf == null) break;
        }
        if (count != 0) return true;
        else return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object m : c) {
            if (!remove(m)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        menuItems = new MenuItem[DEFAULT_ORDER_LENGTH];
        size = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof TableOrder) {
            TableOrder order = (TableOrder) o;
            return customer.equals(order.customer) & equalsMenues(order);

        } else return false;
    }

    @Override
    public int hashCode() {
        int hash = customer.hashCode();
        for (int i = 0; i < size; i++)
            hash ^= menuItems[i].hashCode();
        return hash;
    }

    @Override
    public MenuItem get(int index) {
        return menuItems[index];
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        if (index > 0 & index < menuItems.length) {
            MenuItem item = menuItems[index];
            menuItems[index] = element;
            return item;
        }
        return null;
    }

    @Override
    public void add(int index, MenuItem element) {
        if (index > -1) {
            MenuItem[] buff = new MenuItem[size - index];
            System.arraycopy(menuItems, index, buff, 0, buff.length);
            MenuItem[] items = new MenuItem[menuItems.length + 1];
            System.arraycopy(menuItems, 0, items, 0, index);
            size = (byte) (index);
            add(element);
            for (int i = 0; i < buff.length; i++) {
                add(buff[i]);
            }
        }
    }

    @Override
    public MenuItem remove(int index) {
        if (index < 0) throw new IllegalArgumentException();
        if (index > size) throw new IllegalArgumentException();
        MenuItem o = menuItems[index];
        if (remove(o)) {
            return o;
        } else return null;
    }

    @Override
    public int indexOf(Object o) {
        MenuItem item = (MenuItem) o;
        for (int i = 0; i < size; i++) {
            if (item.equals(menuItems[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        MenuItem item = (MenuItem) o;
        for (int i = size; i > 0; i--) {
            if (item.equals(menuItems[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<MenuItem> listIterator() {
        return new OrderListIterator(this);
    }

    @Override
    public ListIterator<MenuItem> listIterator(int index) {
        return new OrderListIterator(this, index);
    }

    @Override
    public List<MenuItem> subList(int fromIndex, int toIndex) {
        if ((fromIndex < 0) || (toIndex > size) || (fromIndex > size) || (toIndex < 0) || (fromIndex > toIndex))
            throw new IllegalArgumentException();
        LinkedList<MenuItem> list = new LinkedList<MenuItem>();
        for (int i = fromIndex; i <= toIndex; i++)
            list.add(menuItems[i]);
        return list;
    }

    public int getItemsByType(MenuItem item) {
        int result = 0;
        if (item == null) {
            return 0;
        }

        String className = item.getClass().getName();
        for (int i = 0; i < size; i++) {
            if (isEqualsTypes(menuItems[i], className))
                result++;
        }
        return result;
    }

    public int getItemCount(String name) {
        int result = 0;
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], name)) {
                result++;
            }
        }
        return result;
    }

    private byte getSize() {
        for (byte i = 0; i < menuItems.length; i++) {
            if (menuItems[i] == null) return i;
        }
        return -1;
    }

    private boolean equalsMenues(TableOrder order) {
        int addFlag = 0;
        MenuItem[] tmpArr1 = new MenuItem[size];
        MenuItem[] tmpArr2 = new MenuItem[order.size];
        System.arraycopy(tmpArr1, 0, menuItems, 0, size);
        System.arraycopy(tmpArr2, 0, order.menuItems, 0, size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < order.size; j++) {
                if (tmpArr2[j] != null) {
                    if (tmpArr1[i].equals(tmpArr2[j])) {
                        addFlag = 1;
                        tmpArr1[i] = tmpArr2[j] = null;
                        break;
                    }
                }
            }
            if (addFlag == 0) {
                return false;
            } else {
                addFlag = 0;
            }
        }
        return true;
    }

    private boolean isEqualsTypes(MenuItem item, String className) {
        return (className.equalsIgnoreCase(item.getClass().getName()));
    }

    private boolean isEqualsNames(MenuItem item, String name) {
        return (name.equalsIgnoreCase(item.getName()));
    }

    private void checkLaw(MenuItem item) {
        if (item.getClass().getName().equalsIgnoreCase("Drink")) {
            if (customer.getAge() < 18 | LocalDateTime.now().getHour() > 22) {
                throw new UnlawfulActionException("Outlaw");
            }
        }
    }

    private boolean isExist(MenuItem item) {
        for (int i = 0; i < size; i++) {
            if (menuItems[i].equals(item)) return true;
        }
        return false;
    }


}
