package lab5.barBossHouse;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class InternetOrder implements Order {
    private static final Customer DEFAULT_CUSTOMER_VALUE = null;
    private static final int DEFAULT_LENGTH = 0;
    private InternetOrder nextOrder;
    private Customer customer;
    private int itemsCount;
    private ItemsList list;
    private LocalDateTime time;

    public InternetOrder() {
        this(Util.DEFAULT_CUSTOMER_VALUE, new MenuItem[Util.DEFAULT_LENGTH]);
    }

    public InternetOrder(Customer customer, MenuItem[] menuItems) {
        this.customer = customer;
        this.itemsCount = menuItems.length;
        this.time = LocalDateTime.now();
        //this.list = Util.DEFAUL_ITEMSLIST_VALUE;
        addItems(menuItems);
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public LocalDateTime getTime() {
        return this.time;
    }

    public boolean enqueue(InternetOrder order) {
        if (nextOrder == null) {
            this.nextOrder = order;
            return true;
        } else {
            if (order.equals(nextOrder)) throw new AlreadyAddedException("Order already added");
            return nextOrder.enqueue(order);
        }
    }

    public InternetOrder dequeue() {
        InternetOrder result = new InternetOrder();
        result.list = nextOrder.list;
        result.customer = nextOrder.customer;
        nextOrder.shiftQueue();
        return result;
    }

    public InternetOrder getFirst() {
        InternetOrder result = new InternetOrder();
        result.list = nextOrder.list;
        result.customer = nextOrder.customer;
        return result;
    }

    public InternetOrder getLast() {
        if (nextOrder == null) {
            return this;
        } else return nextOrder.getLast();
    }

    public InternetOrder removeLast() {
        if (nextOrder == null) {
            return null;
        } else {
            InternetOrder ord = nextOrder;
            nextOrder = nextOrder.nextOrder;
            return ord;
            //count--;
        }
    }

    public boolean removeOrder(Order order) {
        if (nextOrder == null) {
            return false;
        } else {
            if (nextOrder.equals(order)) {
                nextOrder = nextOrder.nextOrder;
                return true;
            } else return nextOrder.removeOrder(order);
        }
    }


    public InternetOrder getOrder(int indx) {
        if (nextOrder != null) {
            if (indx > 1) {
                return nextOrder.getOrder(--indx);
            } else return nextOrder;
        } else return null;
    }

    public boolean addItem(MenuItem item) {
        return list.add(item);
    }

    public boolean removeItem(MenuItem item) {
        return list.remove(item);
    }

    public boolean removeItem(String itemName) {
        return list.remove(itemName);
    }

    public int removeAllItems(String itemName) {
        return list.removeAll(itemName);
    }

    public int removeAllItems(MenuItem item) {
        return list.removeAll(item);
    }

    @Override
    public int getItemsCount() {
        return list.getItemsCount();
    }

    public MenuItem[] getItems() {
        return list.getItems();
    }

    public double getSummaryCost() {
        return list.getSummaryCost();
    }

    public int getItemCount(String itemName) {
        return list.getItemCount(itemName);
    }

    public int getItemsByType(MenuItem item) {
        String typeName = item.getClass().getName();
        return list.getItemsCountByType(typeName);
    }

    public String[] getItemsNames() {
        return list.getItemsNames();
    }

    public MenuItem[] getItemsSortedToLow() {
        return list.getItemsSortedToLow();
    }

    public void setCustomer(Customer customer) {
        customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InternetOrder: ");
        builder.append(customer.toString());
        builder.append("\n");
        builder.append(list.getItemsCount());
        builder.append(list.toString());
        /*String tmp = "InternetOrder: " + customer.toString() + "\n" + list.getItemsCount();
        tmp += list.toString();*/
        return builder.toString();
    }

    @Override
    public int size() {
        return list.getItemsCount();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        MenuItem item = (MenuItem) o;
        return list.contains(item);
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return new OrderIterator(this);
    }

    @Override
    public Object[] toArray() {
        return list.getItems();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(MenuItem menuItem) {
        return list.add(menuItem);
    }

    @Override
    public boolean remove(Object o) {
        MenuItem item = (MenuItem) o;
        return remove(item);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++) {
            if (!contains(array[i])) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++) {
            if (!add((MenuItem) array[i])) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++) {
            if (!list.add((MenuItem) array[i], index)) return false;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++) {
            if (!remove(array[i])) return false;
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        //???
        return true;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof InternetOrder) {

            InternetOrder order = (InternetOrder) o;
            //if(this.time.equals(order.time)) return false;
            MenuItem[] items = getItems();
            MenuItem[] orderItems = order.getItems();
            if (customer.equals(order.customer) & itemsCount == order.itemsCount & this.time.equals(order.time)) {
                boolean isAdded = false;
                for (int i = 0; i < itemsCount; i++) {
                    for (int j = 0; j < itemsCount; j++) {
                        if (items[i].equals(orderItems[j])) {
                            isAdded = true;
                        }
                    }
                    if (!isAdded) {
                        return false;
                    } else {
                        isAdded = false;
                    }
                }
                return true;
            } else return false;
        } else return false;
    }

    @Override
    public int hashCode() {
        return itemsCount ^ customer.hashCode() ^ list.hashCode();
    }

    @Override
    public MenuItem get(int index) {
        return list.get(index);
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        return list.set(element, index);
    }

    @Override
    public void add(int index, MenuItem element) {
        list.add(element, index);
    }

    @Override
    public MenuItem remove(int index) {
        return list.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @Override
    public ListIterator<MenuItem> listIterator() {
        return new OrderListIterator(this);
    }

    @Override
    public ListIterator<MenuItem> listIterator(int index) {
        return new OrderListIterator(this, index);
    }

    @Override
    public List<MenuItem> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }

    private void addElement(InternetOrder order) {
        if (nextOrder != null) {
            nextOrder.addElement(order);
        } else nextOrder = order;
    }

    private void removeThis() {
        list = null;
        customer = null;
        itemsCount = 0;
    }

    private void shiftQueue() {
        if (nextOrder != null) {
            list = nextOrder.list;
            customer = nextOrder.customer;
            nextOrder = nextOrder.nextOrder;
        }
    }

    private void addItems(MenuItem[] items) {
        if (items.length > 0) {
            list = new ItemsList(null);
            for (int i = 0; i < items.length; i++) {
                list.add(items[i]);
            }
        } else list = new ItemsList(null);

    }
}
