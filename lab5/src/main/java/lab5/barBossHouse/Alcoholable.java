package lab5.barBossHouse;

public interface Alcoholable {
    public boolean isAlcohol();

    public int getAlcoholPercent();
}
