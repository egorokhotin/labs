package lab5.barBossHouse;

import java.util.List;

public interface Order extends List<MenuItem> {

    public boolean addItem(MenuItem item);

    public boolean removeItem(String itemName);

    public boolean removeItem(MenuItem item);

    public int removeAllItems(String itemName);

    public int removeAllItems(MenuItem item);

    public int getItemsCount();

    public MenuItem[] getItems();

    public double getSummaryCost();

    public int getItemCount(String itemName);

    public int getItemsByType(MenuItem item);

    public String[] getItemsNames();

    public MenuItem[] getItemsSortedToLow();

    public Customer getCustomer();

    public void setCustomer(Customer customer);

//    public double getPrice();

    public String toString();
}
