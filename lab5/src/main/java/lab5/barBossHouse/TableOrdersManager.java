package lab5.barBossHouse;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;

public class TableOrdersManager implements OrdersManager, List<Order> {
    private static final int DEFAULT_CAPACITY_VALUE = 0;
    private TableOrder[] tableOrders;
    private int capacity;

    public TableOrdersManager(int tablesCount) {
        if (tablesCount < 0) throw new NegativeSizeException("Incorrect tablesCount");
        tableOrders = new TableOrder[tablesCount];
        capacity = DEFAULT_CAPACITY_VALUE;
    }

    public int getCompletedOrdersCount(LocalDateTime date) {
        TableOrder[] orders = getOrders();
        int result = 0;
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].getTime().getDayOfYear() == date.getDayOfYear())
                result++;
        }
        return result;
    }

    public LinkedList<Order> getCompletedOrders(LocalDateTime date) {
        TableOrder[] orders = getOrders();
        LinkedList<Order> result = new LinkedList<Order>();
        for (int i = 0; i < orders.length; i++) {

            if (orders[i].getTime().getDayOfYear() == date.getDayOfYear()) {
                result.add(orders[i]);
            }
        }
        return result;
    }

    public LinkedList<Order> getCustomerOrders(Customer customer) {
        TableOrder[] orders = getOrders();
        LinkedList<Order> result = new LinkedList<Order>();
        for (int i = 0; i < orders.length; i++) {

            if (orders[i].getCustomer().equals(customer)) {
                result.add(orders[i]);
            }
        }
        return result;
    }

    public void addOrder(int tableNumber, TableOrder newTableOrder) {
        if (newTableOrder != null & isValidNumber(tableNumber)) {
            tableOrders[tableNumber] = newTableOrder;
            capacity++;
        } else throw new AlreadyAddedException("this table is busy!");
    }

    public TableOrder getOrder(int tableNumber) {
        if (isValidNumber(tableNumber)) {
            return tableOrders[tableNumber];
        } else return null;
    }

    public boolean addItemToOrder(int tableNumber, MenuItem menuItem) {
        if (menuItem != null) {
            if (isValidNumber(tableNumber)) {
                tableOrders[tableNumber].addItem(menuItem);
                return true;
            }
        }
        return false;
    }

//    public int getCommonItems(MenuItem item)
//    {
//        int count = 0;
//        int[] ordersIndexes = getBusyTables();
//        for(int i=1; i<ordersIndexes.length; i++)
//        {
//            count += tableOrders[ordersIndexes[i]].getItemsByType(item);
//        }
//
//        return count;
//    }

    public int removeOrder(TableOrder order) {
        int[] ordersIndexes = getBusyTables();
        for (int i = 0; i < ordersIndexes.length; i++) {
            if (tableOrders[ordersIndexes[i]].equals(order)) {
                tableOrders[ordersIndexes[i]] = null;
                capacity--;
                return ordersIndexes[i];
            }
        }
        return -1;
    }

    public int removeAllOrders(TableOrder order) {
        int[] ordersIndexes = getBusyTables();
        int count = 0;
        for (int i = 0; i < ordersIndexes.length; i++) {
            if (tableOrders[ordersIndexes[i]].equals(order)) {
                tableOrders[ordersIndexes[i]] = null;
                capacity--;
                count++;
            }
        }
        if (count != 0) return count;
        else return -1;
    }

    public void freeTable(int tableNumber) {
        if (isValidNumber(tableNumber)) {
            tableOrders[tableNumber] = null;
            capacity--;
        }
    }

    public int getFirstFreeTableNumber() {
        for (int i = 0; i < tableOrders.length; i++) {
            if (tableOrders[i] == null) return i;
        }

        throw new NoFreeTableException("all tables are busy");
    }

    public int[] getFreeTables() {
        return getTables(x -> x != null);
    }

    public int[] getBusyTables() {
        return getTables(x -> x == null);
    }

    public TableOrder[] getOrders() {
        TableOrder[] result = null;
        if (capacity != 0) {
            result = new TableOrder[capacity];
            int j = 0;
            for (int i = 0; i < tableOrders.length; i++) {
                if (tableOrders[i] != null) {
                    result[j] = tableOrders[i];
                    j++;
                }
            }
            return result;
        }
        return result;
    }

    public int getSummaryItemCount(String name) {
        int[] freeIndxs = getBusyTables();
        int result = 0;
        for (int i = 0; i < freeIndxs.length; i++) {
            result += tableOrders[freeIndxs[i]].getItemCount(name);
        }
        return result;
    }

    public int getSummaryItemCountByType(MenuItem item) {
        int[] freeIndxs = getBusyTables();
        int result = 0;
        for (int i = 0; i < freeIndxs.length; i++) {
            result += tableOrders[freeIndxs[i]].getItemsByType(item);
        }
        return result;
    }

    private int[] getTables(Predicate<Order> predicate) {
        int[] result = null;
        if (capacity != 0) {
            result = new int[capacity];
            int j = 0;
            for (int i = 0; i < tableOrders.length; i++) {
                if (predicate.test(tableOrders[i])) {
                    result[j] = i;
                    j++;
                }
            }
        }
        return result;
    }

    public double getSummaryCost() {
        double sum = 0;
        TableOrder[] ordersTempArray = getOrders();
        if (ordersTempArray != null) {
            for (int i = 0; i < ordersTempArray.length; i++) {
                sum += ordersTempArray[i].getSummaryCost();
            }
        }
        return sum;
    }

    public int getOrdersCount() {
        return capacity;
    }

    private boolean isValidNumber(int number) {
        if (number > -1 & number < tableOrders.length) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends Order> c) {
        Object[] orders = c.toArray();
        for (int i = 0; i < orders.length; i++) {
            if (!add((Order) orders[i])) return false;
        }
        return true;
    }

    @Override
    public Order get(int index) {
        if (index > 0 & index < tableOrders.length)
            return tableOrders[index];
        else return null;
    }

    @Override
    public Order set(int index, Order element) {
        if (index > 0 & index < tableOrders.length) {
            Order ord = tableOrders[index];
            tableOrders[index] = (TableOrder) element;
            return ord;
        }
        return null;
    }

    @Override
    public void add(int index, Order element) {
        if (index >= 0 & index < tableOrders.length) {
            addOrder(index, (TableOrder) element);
        }
    }

    @Override
    public Order remove(int index) {
        if (index >= 0 & index < tableOrders.length) {
            Order ord = tableOrders[index];
            freeTable(index);
            return ord;
        }
        return null;
    }

    @Override
    public int indexOf(Object o) {
        //Order[] orders = getOrders();
        Order ord = (Order) o;
        for (int i = 0; i < tableOrders.length; i++) {
            if (tableOrders[i].equals(ord)) return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Order ord = (Order) o;
        for (int i = tableOrders.length; i > 0; i--) {
            if (tableOrders[i].equals(ord)) return i;
        }
        return -1;
    }

    @Override
    public ListIterator<Order> listIterator() {
        return new OrdersManagerListIterator(this);
    }

    @Override
    public ListIterator<Order> listIterator(int index) {
        return new OrdersManagerListIterator(this, index);
    }

    @Override
    public List<Order> subList(int fromIndex, int toIndex) {
        if (fromIndex > toIndex || fromIndex < 0 || toIndex > tableOrders.length - 1) return null;
        LinkedList<Order> orders = new LinkedList<>();
        for (int i = fromIndex; i <= toIndex; i++)
            orders.add(tableOrders[i]);
        return orders;
    }

    @Override
    public int size() {
        return getOrdersCount();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        //Order ord = (Order)o;
        for (int i = 0; i < tableOrders.length; i++) {
            if (tableOrders[i].equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<Order> iterator() {
        return new OrdersManagerIterator(this);
    }

    @Override
    public Object[] toArray() {
        return getOrders();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Order menuItems) {
        int num = getFirstFreeTableNumber();
        addOrder(num, (TableOrder) menuItems);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int n = indexOf(o);
        if (n >= 0) {
            freeTable(n);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Object[] orders = c.toArray();
        for (int i = 0; i < orders.length; i++) {
            if (!contains(orders[i])) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        Object[] orders = c.toArray();
        for (int i = 0; i < orders.length; i++) {
            if (!add((Order) orders[i])) return false;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] orders = c.toArray();
        for (int i = 0; i < orders.length; i++) {
            if (!remove(orders[i])) return false;
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        tableOrders = new TableOrder[tableOrders.length];
    }
}
