package lab5.barBossHouse;

public class UnlawfulActionException extends RuntimeException {
    public UnlawfulActionException(String message) {
        super(message);
    }
}
