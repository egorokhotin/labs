package lab5.barBossHouse;

public class Drink extends MenuItem implements Alcoholable {
    private final DrinkTypeEnum drinkType;
    private final int alcoholPercent;

    public Drink(DrinkTypeEnum drinkType, String name) {
        this(name, Util.DEFAULT_STRING, DEFAULT_PRICE, drinkType);
    }

    public Drink(String drinkName, String drinkDescription, double drinkPrice, DrinkTypeEnum drinkType) {
        this(drinkName, drinkDescription, drinkPrice, drinkType, Util.DEFAULT_ALCOHOL_PERCENT);
    }

    public Drink(String drinkName, String drinkDescription, double drinkPrice, DrinkTypeEnum drinkType, int alcoholPercent) {
        super(drinkName, drinkDescription, drinkPrice);
        if (alcoholPercent < 0 | alcoholPercent > 100)
            throw new IllegalArgumentException("alcoholPercent is incorrect!");
        this.drinkType = drinkType;
        this.alcoholPercent = alcoholPercent;
    }

    public boolean isAlcohol() {
        if (alcoholPercent > 0) return true;
        else return false;
    }

    public int getAlcoholPercent() {
        return alcoholPercent;
    }

    @Override
    public String toString() {
        return String.format("Drink: %s%s%s%s",
                getDrinkTypeString(),
                super.toString(),
                getAlcoholPercentString(),
                super.getDescription());
        /*return "Drink: " +
                getDrinkTypeString() +
                super.toString() +
                getAlcoholPercentString() +
                super.getDescription();*/
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof Drink) {
            Drink drink = (Drink) o;
            return (alcoholPercent == drink.alcoholPercent) &
                    super.equals(drink);
        } else return false;
    }

    @Override
    public int hashCode() {
        return alcoholPercent ^ drinkType.hashCode() ^ super.hashCode();
    }

    private String getAlcoholPercentString() {
        if (alcoholPercent == Util.DEFAULT_ALCOHOL_PERCENT) {
            return "";
        } else return "Alcohol: " + String.valueOf(alcoholPercent) + "%.";
    }

    private String getDrinkTypeString() {
        if (drinkType == null) {
            return "";
        } else return drinkType.toString() + " ";
    }
}
