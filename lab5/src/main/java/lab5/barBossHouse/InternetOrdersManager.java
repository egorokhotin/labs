package lab5.barBossHouse;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class InternetOrdersManager implements OrdersManager, Deque<Order> {
    private InternetOrder order;
    private int count;

    public InternetOrdersManager() {
        this(new InternetOrder[Util.DEFAULT_LENGTH]);
    }

    public InternetOrdersManager(InternetOrder[] orders) {
        parseOrdersArray(orders);
        this.count = Util.DEFAULT_COUNT_VALUE;
    }

    public boolean enqueOrder(InternetOrder order) {
        count++;
        boolean result = this.order.enqueue(order);
        return result;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean contains(Object o) {
        Order ord = (Order) o;
        Order[] orders = getOrders();
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<Order> iterator() {
        return new OrdersManagerIterator(this);
    }

    @Override
    public Iterator<Order> descendingIterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Order menuItems) {
        return enqueOrder((InternetOrder) menuItems);
    }

    @Override
    public boolean offer(Order menuItems) {
        return false;
    }

    @Override
    public Order remove() {
        return dequeue();
    }

    @Override
    public Order poll() {
        return dequeue();
    }

    @Override
    public Order element() {
        return order.getFirst();
    }

    @Override
    public Order peek() {
        return order.getFirst();
    }

    @Override
    public void push(Order menuItems) {
        add((InternetOrder) menuItems);
    }

    @Override
    public Order pop() {
        return dequeue();
    }

    @Override
    public boolean remove(Object o) {
        return order.removeOrder((Order) o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++)
            if (!contains(array[i])) return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++)
            if (!add((Order) array[i])) return false;
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] array = c.toArray();
        for (int i = 0; i < array.length; i++)
            if (!remove((Order) array[i])) return false;
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        order = new InternetOrder();
        count = Util.DEFAULT_COUNT_VALUE;
    }

    public int getCompletedOrdersCount(LocalDateTime date) {
        InternetOrder[] orders = getOrders();
        int result = 0;
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].getTime().getDayOfYear() == date.getDayOfYear())
                result++;
        }
        return result;
    }

    public LinkedList<Order> getCompletedOrders(LocalDateTime date) {
        InternetOrder[] orders = getOrders();
        LinkedList<Order> result = new LinkedList<Order>();
        for (int i = 0; i < orders.length; i++) {

            if (orders[i].getTime().getDayOfYear() == date.getDayOfYear()) {
                result.add(orders[i]);
            }
        }
        return result;
    }

    public LinkedList<Order> getCustomerOrders(Customer customer) {
        InternetOrder[] orders = getOrders();
        LinkedList<Order> result = new LinkedList<Order>();
        for (int i = 0; i < orders.length; i++) {

            if (orders[i].getCustomer().equals(customer)) {
                result.add(orders[i]);
            }
        }
        return result;
    }

    @Override
    public void addFirst(Order menuItems) {
        InternetOrder ord = (InternetOrder) menuItems;

    }

    @Override
    public void addLast(Order menuItems) {
        enqueOrder((InternetOrder) menuItems);
    }

    @Override
    public boolean offerFirst(Order menuItems) {
        addFirst(menuItems);
        return true;
    }

    @Override
    public boolean offerLast(Order menuItems) {
        addLast(menuItems);
        return true;
    }

    @Override
    public Order removeFirst() {
        return dequeue();
    }

    @Override
    public Order removeLast() {
        return order.removeLast();
    }

    @Override
    public Order pollFirst() {
        return dequeue();
    }

    @Override
    public Order pollLast() {
        return order.getLast();
    }

    public InternetOrder getFirst() {
        return order.getFirst();
    }

    @Override
    public Order getLast() {
        Order[] orders = getOrders();
        if (orders != null & orders.length > 0) {
            return orders[orders.length - 1];
        }
        return null;
    }

    @Override
    public Order peekFirst() {
        return getFirst();
    }

    @Override
    public Order peekLast() {
        return getLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    public InternetOrder dequeue() {
        count--;
        return order.dequeue();
    }

    public int getOrdersCount() {
        return count;
    }

    public InternetOrder[] getOrders() {
        InternetOrder[] array = new InternetOrder[count];
        for (int i = Util.FIRST_QUEUE_POSITION, j = 0; j < array.length; j++, i++)
            array[j] = order.getOrder(i);
        return array;
    }

    public double getSummaryCost() {
        InternetOrder[] tmp = getOrders();
        double result = 0;
        if (tmp != null) {
            for (int i = 0; i < tmp.length; i++)
                result += tmp[i].getSummaryCost();
        }
        return result;
    }

    public int getSummaryItemCount(String itemName) {
        InternetOrder[] tmp = getOrders();
        int result = 0;
        if (tmp != null) {
            for (int i = 0; i < tmp.length; i++) {
                result += tmp[i].getItemCount(itemName);
            }
        }
        return result;
    }

    public int getSummaryItemCountByType(MenuItem item) {
        InternetOrder[] tmp = getOrders();
        int result = 0;
        if (tmp != null) {
            for (int i = 0; i < tmp.length; i++) {
                result += tmp[i].getItemsByType(item);
            }
        }
        return result;
    }

    private void parseOrdersArray(InternetOrder[] orders) {
        order = new InternetOrder();
        for (int i = 0; i < orders.length; i++) {
            count++;
            order.enqueue(orders[i]);
        }
    }


}
