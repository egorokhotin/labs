package lab5.barBossHouse;

public class NegativeSizeException extends java.lang.NegativeArraySizeException {
    public NegativeSizeException(String message) {
        super(message);
    }
}
