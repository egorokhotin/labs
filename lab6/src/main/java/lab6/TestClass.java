package lab6;

public class TestClass {
    private static final int DEFAULT_ORDERS_COUNT = 10;

    //Test TableOrdersManager
    public String tableOrdersManager_addOrder_ReturnTrue() {
        String result = "tableOrdersManager_addOrder: ";
        TableOrdersManager manager = new TableOrdersManager(DEFAULT_ORDERS_COUNT);
        manager.addOrder(1, new TableOrder());
        manager.addOrder(3, new TableOrder());
        manager.addOrder(5, new TableOrder());
        boolean b = (manager.getBusyTables().length == 3);
        return result + b;
    }

    public String tableOrdersManager_removeOrder_ReturnTrue() {
        String result = "tableOrdersManager_removeOrder: ";
        TableOrdersManager manager = new TableOrdersManager(DEFAULT_ORDERS_COUNT);
        manager.addOrder(1, new TableOrder());
        manager.addOrder(3, new TableOrder());
        manager.addOrder(5, new TableOrder());

        manager.removeOrder(new TableOrder());

        boolean b = (manager.getBusyTables().length == 2) & (manager.getBusyTables()[0] == 3);
        return result + b;
    }

    public String tableOrdersManager_removeAllOrders_ReturnTrue() {
        String result = "tableOrdersManager_removeAllOrders: ";
        TableOrdersManager manager = new TableOrdersManager(DEFAULT_ORDERS_COUNT);
        manager.addOrder(1, new TableOrder());
        manager.addOrder(3, new TableOrder());
        manager.addOrder(5, new TableOrder());

        manager.removeAllOrders(new TableOrder());

        boolean b = (manager.getBusyTables() == null);
        return result + b;
    }

    public String tableOrdersManager_getSummaryCost_ReturnTrue() {
        String result = "tableOrdersManager_getSummaryCost: ";
        TableOrdersManager manager = new TableOrdersManager(DEFAULT_ORDERS_COUNT);
        manager.addOrder(1, new TableOrder());
        manager.addOrder(3, new TableOrder());
        manager.addOrder(5, new TableOrder());

        Drink drink0 = new Drink("tea", "some tea", 10, DrinkTypeEnum.BLACK_TEA);
        Drink drink1 = new Drink("beer", "some beer", 15, DrinkTypeEnum.BEER);
        Drink drink2 = new Drink("coffee", "some coffe", 20, DrinkTypeEnum.COFEE);

        Dish dish0 = new Dish("Eggs", "some eggs", 10);
        Dish dish1 = new Dish("Potatoes", "some potatoes", 15);
        Dish dish2 = new Dish("Boiled Corn", "some corn", 20);

        manager.addItemToOrder(1, drink0);
        manager.addItemToOrder(1, dish0);
        manager.addItemToOrder(3, drink1);
        manager.addItemToOrder(3, dish1);
        manager.addItemToOrder(5, drink2);
        manager.addItemToOrder(5, dish2);

        boolean b = (manager.getSummaryCost() == 90);

        return result + b;
    }

    public String tableOrdersManager_getSummaryItemCount_ByType() {
        String result = "tableOrdersManager_getSummaryItemCount_ByType: ";
        TableOrdersManager manager = new TableOrdersManager(DEFAULT_ORDERS_COUNT);
        manager.addOrder(1, new TableOrder());
        manager.addOrder(3, new TableOrder());
        manager.addOrder(5, new TableOrder());

        Drink drink0 = new Drink("tea", "some tea", 10, DrinkTypeEnum.BLACK_TEA);
        Drink drink1 = new Drink("beer", "some beer", 15, DrinkTypeEnum.BEER);
        Drink drink2 = new Drink("coffee", "some coffe", 20, DrinkTypeEnum.COFEE);

        Dish dish0 = new Dish("Eggs", "some eggs", 10);
        Dish dish1 = new Dish("Potatoes", "some potatoes", 15);
        //Dish dish2 = new Dish("Boiled Corn", "some corn",20);

        manager.addItemToOrder(1, drink0);
        manager.addItemToOrder(1, dish0);
        manager.addItemToOrder(3, drink1);
        manager.addItemToOrder(3, dish1);
        manager.addItemToOrder(5, drink2);
        manager.addItemToOrder(5, dish0);

        boolean b = (manager.getSummaryItemCountByType(drink0) == 3);

        return result + b;
    }

    public String tableOrdersManager_getItemCount_ByName() {
        String result = "tableOrdersManager_getSummaryItemCount_ByType: ";
        TableOrdersManager manager = new TableOrdersManager(DEFAULT_ORDERS_COUNT);
        manager.addOrder(1, new TableOrder());
        manager.addOrder(3, new TableOrder());
        manager.addOrder(5, new TableOrder());

        Drink drink0 = new Drink("tea", "some tea", 10, DrinkTypeEnum.BLACK_TEA);
        Drink drink1 = new Drink("beer", "some beer", 15, DrinkTypeEnum.BEER);
        Drink drink2 = new Drink("coffee", "some coffe", 20, DrinkTypeEnum.COFEE);

        Dish dish0 = new Dish("Eggs", "some eggs", 10);
        Dish dish1 = new Dish("Potatoes", "some potatoes", 15);
        //Dish dish2 = new Dish("Boiled Corn", "some corn",20);

        manager.addItemToOrder(1, drink0);
        manager.addItemToOrder(1, dish0);
        manager.addItemToOrder(3, drink1);
        manager.addItemToOrder(3, dish1);
        manager.addItemToOrder(5, drink2);
        manager.addItemToOrder(5, dish0);

        boolean b = (manager.getSummaryItemCount(dish0.getName()) == 2);

        return result + b;
    }

    //Test InternetOrdersManager
    public String internetOrdersManager_enqueueOrder() {
        String result = "internetOrdersManager_addOrder: ";
        InternetOrdersManager manager = new InternetOrdersManager();//(DEFAULT_ORDERS_COUNT);
        manager.enqueOrder(new InternetOrder());
        manager.enqueOrder(new InternetOrder());
        manager.enqueOrder(new InternetOrder());
        boolean b = (manager.getOrders().length == 3);
        return result + b;
    }

    public String internetOrdersManager_dequeueOrder() {
        String result = "internetOrdersManager_removeOrder: ";
        InternetOrdersManager manager = new InternetOrdersManager();//(DEFAULT_ORDERS_COUNT);
        //manager.enqueOrder(new InternetOrder(new Customer(18), new MenuItem[]{new Drink(DrinkTypeEnum.BLACK_TEA,"tea")}));
        manager.enqueOrder(new InternetOrder());
        manager.enqueOrder(new InternetOrder());

        InternetOrder order = manager.dequeue();

        boolean b = (order.getItems().length == 1) & (manager.getOrders().length == 2);
        return result + b;
    }

    public String internetOrdersManager_getSummaryItemCount() {
        String result = "internetOrdersManager_getSummaryItemCount: ";
        InternetOrdersManager manager = new InternetOrdersManager();//(DEFAULT_ORDERS_COUNT);
//        manager.enqueOrder(new InternetOrder(new Customer(18), new MenuItem[]{new Drink("tea","some tea", 10, DrinkTypeEnum.BLACK_TEA)}));
//        manager.enqueOrder(new InternetOrder(new Customer(20), new MenuItem[]{new Drink("beer","some beer", 15,DrinkTypeEnum.BEER)}));
//        manager.enqueOrder(new InternetOrder(new Customer(22), new MenuItem[]{new Drink("tea","some tea", 10, DrinkTypeEnum.BLACK_TEA)}));


        boolean b = manager.getSummaryItemCount("tea") == 2;
        return result + b;
    }

    public String internetOrdersManager_getSummaryItemCount_ByType() {
        String result = "internetOrdersManager_getSummaryItemCount_ByType: ";
        InternetOrdersManager manager = new InternetOrdersManager();//(DEFAULT_ORDERS_COUNT);
//        manager.enqueOrder(new InternetOrder(new Customer(18), new MenuItem[]{new Dish("Eggs", "some eggs",10)}));
//        manager.enqueOrder(new InternetOrder(new Customer(20), new MenuItem[]{new Drink("beer","some beer", 15,DrinkTypeEnum.BEER)}));
//        manager.enqueOrder(new InternetOrder(new Customer(22), new MenuItem[]{new Dish("Eggs", "some eggs",10)}));


        boolean b = manager.getSummaryItemCountByType(new Drink("beer", "some beer", 15, DrinkTypeEnum.BEER)) == 1;
        return result + b;
    }


    //TestAddress

}
