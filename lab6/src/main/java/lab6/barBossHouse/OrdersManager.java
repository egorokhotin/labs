package lab6.barBossHouse;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;

public interface OrdersManager extends Collection<Order> {
    public int getOrdersCount();

    public Order[] getOrders();

    public double getSummaryCost();

    public int getSummaryItemCount(String itemName);

    public int getSummaryItemCountByType(MenuItem item);

    public int getCompletedOrdersCount(LocalDateTime date);

    public LinkedList<Order> getCompletedOrders(LocalDateTime date);

    public LinkedList<Order> getCustomerOrders(Customer customer);
}

