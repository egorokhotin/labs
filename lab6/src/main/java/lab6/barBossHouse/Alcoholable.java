package lab6.barBossHouse;

public interface Alcoholable {
    public boolean isAlcohol();

    public int getAlcoholPercent();
}
