package lab6.barBossHouse;

import java.time.LocalDateTime;

public class Customer {
    private final String firstName;
    private final String secondName;
    private final LocalDateTime birthDate;
    private final Address address;

    public static final Customer MATURE_UNKNOWN_CUSTOMER = new Customer(LocalDateTime.now().minusYears(21));
    public static final Customer NOT_MATURE_UNKNOWN_CUSTOMER = new Customer(LocalDateTime.now().minusYears(14));

    public Customer() {
        this(LocalDateTime.now());
    }

    public Customer(LocalDateTime birthDate) {
        this(Util.DEFAULT_STRING, Util.DEFAULT_STRING, birthDate, new Address());
    }

    public Customer(String firstName, String secondName, LocalDateTime birthDate, Address address) {
        if (birthDate.compareTo(LocalDateTime.now()) > 0) throw new IllegalArgumentException("birthDate is incorrect");
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return LocalDateTime.now().getYear() - birthDate.getYear();
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Customer: ");
        builder.append(getStringName());
        builder.append(getStringAge());
        builder.append(address.toString());
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof Customer) {
            Customer customer = (Customer) o;
            return firstName.equalsIgnoreCase(customer.getFirstName()) &
                    secondName.equalsIgnoreCase((customer.getSecondName())) &
                    (birthDate.isEqual(customer.birthDate)) &
                    address.equals(customer.getAddress());
        } else return false;
    }

    @Override
    public int hashCode() {
        return getAge() ^ firstName.hashCode() ^
                secondName.hashCode() ^
                address.hashCode();
    }

    private String getStringName() {
        String result = "";
        result += parseValue(secondName);
        result += parseValue(firstName);
        result += ", ";
        return result;
    }

    private String getStringAge() {
        String result = "";
        if (parseValue(getAge()).equalsIgnoreCase("")) {
            return "";
        } else return parseValue(getAge()) + ", ";
    }

    private String parseValue(String s) {
        if (s.equalsIgnoreCase(Util.DEFAULT_STRING)) return "";
        else return s + " ";
    }

    private String parseValue(int num) {
        if (num == Util.DEFAULT_INT) return "";
        else return String.valueOf(num);
    }

}
