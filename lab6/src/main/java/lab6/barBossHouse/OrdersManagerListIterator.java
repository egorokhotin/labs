package lab6.barBossHouse;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class OrdersManagerListIterator implements ListIterator<Order> {
    private Order[] orders;
    private OrdersManager manager;
    private int index;

    public OrdersManagerListIterator(OrdersManager manager) {
        this(manager, -1);
    }

    public OrdersManagerListIterator(OrdersManager manager, int index) {
        this.index = index;
        this.manager = manager;
        orders = manager.getOrders();
    }

    @Override
    public boolean hasNext() {
        if (index > orders.length - 1 || index < 0) return false;
        return true;
    }

    @Override
    public Order next() {
        if (hasNext()) {
            index++;
            return orders[index];
        } else throw new NoSuchElementException();
    }

    @Override
    public boolean hasPrevious() {
        return index >= 0;
    }

    @Override
    public Order previous() {
        if (hasPrevious()) {
            index--;
            return orders[index];
        } else throw new NoSuchElementException();
    }

    @Override
    public int nextIndex() {
        if (hasNext())
            return index + 1;
        else return orders.length;
    }

    @Override
    public int previousIndex() {
        if (hasPrevious())
            return index - 1;
        else return -1;
    }

    @Override
    public void remove() {
        if (index >= 0 & index < orders.length - 1) {
            manager.remove(orders[index]);
            orders = manager.getOrders();
        }
    }

    @Override
    public void set(Order menuItems) {

    }

    @Override
    public void add(Order menuItems) {

    }
}
