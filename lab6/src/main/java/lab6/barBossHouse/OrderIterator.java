package lab6.barBossHouse;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class OrderIterator implements Iterator<MenuItem> {
    int indx = -1;
    MenuItem[] items;

    public OrderIterator(Order order) {
        items = order.getItems();
    }

    public boolean hasNext() {
        if (indx < items.length - 1) return true;
        else return false;
    }

    public MenuItem next() {
        if (indx < items.length - 1) {
            return items[indx++];
        } else throw new NoSuchElementException();
    }

}
