package lab6.barBossHouse;
//
//import java.awt.*;

import java.util.LinkedList;

public class ItemsList {
    private ItemsList next;
    private MenuItem val;
    private boolean isInit;
    private int indx;

    public ItemsList(MenuItem item) {
        this.val = item;
        this.indx = 0;
        isInit = true;
    }

    public boolean add(MenuItem item) {
        if (isInit) {
            ItemsList element = new ItemsList(item);
            return addElement(element);
        } else {
            this.val = item;
            this.isInit = true;
            return true;
        }
    }

    private boolean addElement(ItemsList element) {
        if (isNextNull()) {
            element.indx = indx + 1;
            next = element;
            return true;
        } else {
            return this.next.addElement(element);
        }
    }

    public boolean remove(MenuItem item) {
        if (isInit) {
            return removeElement(item);
        } else return false;
    }

    public int removeAll(MenuItem item) {
        if (isInit) {
            return removeAllElements(item);
        } else {
            return 0;
        }
    }

    public int removeAll(String name) {
        if (isInit) {
            return removeAllElements(name);
        } else {
            return 0;
        }
    }

    public String[] getItemsNames() {
        if (isInit) {
            String[] names = new String[getNotNullItemsCount()];
            int i = 0;
            next.setName(names, i);
            i = 0;
            i = Util.getNotNullCount(names);
            String[] result = new String[i];
            System.arraycopy(result, 0, names, 0, i);
            return result;

        } else return null;
    }

    public MenuItem[] getItemsSortedToLow() {
        return quickSortItems();
    }

    public MenuItem[] getItems() {
        if (isInit) {
            MenuItem[] items = new MenuItem[getNotNullItemsCount()];
            int i = 0;
            next.setItem(items, i);
            i = Util.getNotNullCount(items);
            MenuItem[] result = new MenuItem[i];
            System.arraycopy(result, 0, items, 0, i);
            return result;
        } else return null;
    }

    public int lastIndexOf(Object o) {
        Integer value = -1;
        if (isInit) {
            MenuItem item = (MenuItem) o;
            getLastIndexOf(item, value);
        }
        return value;
    }

    public int indexOf(Object o) {
        if (isInit) {
            MenuItem item = (MenuItem) o;
            return getIndexOf(item);
        }
        return -1;
    }

    public void clear() {
        if (isInit) {
            next = null;
            val = null;
            isInit = false;
        }
    }

    public boolean contains(MenuItem item) {
        if (isInit) {
            return isContains(item);
        } else return false;
    }

    public boolean add(MenuItem item, int index) {
        if (isInit)
            return addElement(item, index);
        else return false;
    }

    public MenuItem set(MenuItem item, int index) {
        if (isInit) {
            return setElement(item, index);
        } else return null;
    }

    public MenuItem get(int index) {
        if (isInit) {
            ItemsList l = getElement(index);
            if (l != null)
                return l.val;
        }
        return null;
    }

    public LinkedList<MenuItem> subList(int fromIndex, int toIndex) {
        if (isInit) {
            if ((fromIndex < 0) || (fromIndex > toIndex)) return null;
            else return getSubList(fromIndex, toIndex);
        }
        return null;
    }

    public MenuItem remove(int index) {
        if (isInit) {
            return removeItem(index);
        } else return null;
    }

    private boolean isContains(MenuItem item) {
        if (val.equals(item)) return true;
        else {
            if (isNextNull()) return false;
            else return next.isContains(item);
        }
    }

    private LinkedList<MenuItem> getSubList(int fromIndex, int toIndex) {
        LinkedList<MenuItem> list = new LinkedList<>();
        addElement(list, toIndex);
        return list;
    }

    private void addElement(LinkedList<MenuItem> list, int toIndex) {
        if (isNextNull()) {
            if (indx <= toIndex)
                list.add(val);
        } else {
            if (indx <= toIndex)
                list.add(val);
            next.addElement(list, toIndex);
        }
    }

    private boolean addElement(MenuItem item, int index) {
        ItemsList list = getElement(index);
        if (list != null) {
            ItemsList buf = list.next;
            list.next = new ItemsList(item);
            list.next.indx = buf.indx;
            list.next.next = buf;
            list.next.next.incrementIndxs();
            return true;
        } else return false;
    }

    private MenuItem setElement(MenuItem item, int index) {
        ItemsList list = getElement(index);
        if (list != null) {
            MenuItem v = list.next.val;
            list.next.val = item;
            return val;
        } else return null;
    }

    private void setItem(MenuItem[] array, int i) {
        if (isNextNull()) {
            if (val != null) {
                array[i] = val;
                return;
            } else {
                return;
            }
        } else {
            if (val != null) {
                array[i] = val;
                next.setItem(array, ++i);
            } else {
                next.setItem(array, i);
            }
        }
    }

    private MenuItem removeItem(int index) {
        if (isNextNull()) {
            return null;
        } else {
            if (next.indx == index) {
                MenuItem item = next.val;
                next = next.next;
                next.decrementIndxs();
                return item;
            } else return next.removeItem(index);
        }
    }

    private void setName(String[] array, int i) {
        boolean isAdded = false;
        if (isNextNull()) {
            for (int j = 0; j < i; j++) {
                if (array[j].equalsIgnoreCase(val.getName())) {
                    isAdded = true;
                    break;
                }
            }

            if (isAdded) {
                return;
            } else {
                array[i] = val.getName();
                return;
            }
        } else {
            for (int j = 0; j < i; j++) {
                if (array[j].equalsIgnoreCase(val.getName())) {
                    isAdded = true;
                    break;
                }
            }

            if (isAdded) {
                next.setName(array, ++i);
            } else {
                array[i] = val.getName();
                next.setName(array, ++i);
            }
        }
    }

    private boolean removeElement(MenuItem element) {
        if (isNextNull()) {
            return false;
        } else {
            if (next.val.equals(element)) {
                next.next = next.next.next;
                next.next.decrementIndxs();
                return true;
            } else {
                return next.removeElement(element);
            }
        }
    }

    private int removeAllElements(MenuItem item) {
        if (isNextNull()) {
            return 0;
        } else {
            if (next.val.equals(item)) {
                next.next = next.next.next;
                next.next.decrementIndxs();
                return 1 + next.removeAllElements(item);
            } else {
                return next.removeAllElements(item);
            }
        }
    }

    private int removeAllElements(String name) {
        if (isNextNull()) {
            return 0;
        } else {
            if (next.val.getName().equalsIgnoreCase(name)) {
                next.next = next.next.next;
                next.next.decrementIndxs();
                return 1 + next.removeAllElements(name);
            } else {
                return next.removeAllElements(name);
            }
        }
    }

    public int getItemsCount() {
        if (isInit) {
            if (isNextNull()) {
                return 1;
            } else {
                return 1 + next.getItemsCount();
            }
        } else {
            return 0;
        }
    }

    public double getSummaryCost() {
        if (isInit) {
            return calcSummaryCost();
        } else return 0;
    }

    public boolean remove(String itemName) {
        if (isInit) {
            if (isNextNull()) {
                return false;
            } else {
                if (next.val != null) {
                    if (next.val.getName().equalsIgnoreCase(itemName)) {
                        next = next.next;
                        return true;
                    } else return next.remove(itemName);
                } else return false;
            }
        } else return false;
    }

    public int getItemCount(String itemName) {
        if (isInit) {
            return next.calcItemCount(itemName);
        } else return 0;
    }

    public int getItemsCountByType(String typeName) {
        if (isInit) {
            if (isNextNull()) {
                if (val != null) {
                    if (val.getClass().getName().equalsIgnoreCase(typeName))
                        return 1;
                    else return 0;
                } else {
                    return 0;
                }

            } else {
                if (val != null) {
                    if (val.getClass().getName().equalsIgnoreCase(typeName))
                        return 1 + next.getItemsCountByType(typeName);
                    else return next.getItemsCountByType(typeName);
                } else return next.getItemsCountByType(typeName);

            }
        } else return 0;
    }

    public void setValue(MenuItem item) {
        this.val = item;
    }

    public MenuItem getValue(MenuItem item) {
        return this.val;
    }

    public int hashCode() {
        if (isInit) {
            return next.calcHashCode();
        } else return 0;
    }

    public String toString() {
        if (isInit) {
            return next.toStringItems();
        } else return null;
    }

    private void getLastIndexOf(MenuItem item, Integer value) {
        if (isInit) {
            if (this.val.equals(item)) {
                value = indx;
            }

            if (isNextNull()) return;
            else next.getLastIndexOf(item, value);
        }
    }

    private int getIndexOf(MenuItem item) {
        if (isInit) {
            if (this.val.equals(item))
                return indx;

            if (isNextNull()) return -1;
            else return next.getIndexOf(item);
        }
        return -1;
    }

    private int calcItemCount(String itemName) {
        if (isNextNull()) {
            return (val.getName().equalsIgnoreCase(itemName)) ?
                    1 :
                    0;
        } else {
            return (val.getName().equalsIgnoreCase(itemName)) ?
                    1 + next.calcItemCount(itemName) :
                    next.calcItemCount(itemName);

        }
    }

    private double calcSummaryCost() {
        if (isNextNull()) {
            if (val != null) {
                return val.getPrice();
            } else return 0;
        } else {
            if (val != null) {
                return val.getPrice() + next.calcSummaryCost();
            } else return next.calcSummaryCost();
        }
    }

    private String toStringItems() {
        if (isNextNull()) {
            if (val != null) {
                return val.toString();
            } else {
                return "";
            }
        } else {
            if (val != null) {
                return val.toString() + '\n' + next.toStringItems();
            } else return next.toStringItems();
        }
    }

    private int calcHashCode() {
        if (isNextNull()) {
            if (val != null) {
                return val.hashCode();
            } else return 0;
        } else {
            if (val != null) {
                return val.hashCode() ^ next.calcHashCode();
            } else return next.calcHashCode();
        }
    }

    private int getNotNullItemsCount() {
        if (isInit) {
            if (isNextNull()) {
                return (val == null) ? 0 : 1;
            } else {
                return (val == null) ? next.getItemsCount() : 1 + next.getItemsCount();
            }
        } else {
            return 0;
        }
    }

    private ItemsList getElement(int i) {
        if (isNextNull()) {
            return null;
        } else {
            if (next.indx == i) {
                return next;
            } else {
                return next.getElement(i);
            }
        }
    }

    private void decrementIndxs() {
        if (isNextNull()) {
            indx--;
            return;
        } else {
            indx--;
            next.decrementIndxs();
        }
    }

    private void incrementIndxs() {
        if (isNextNull()) {
            indx++;
            return;
        } else {
            indx++;
            next.incrementIndxs();
        }
    }

    private boolean isNextNull() {
        return (next == null);
    }


    private MenuItem[] quickSortItems() {
        MenuItem[] result = getItems();
        Util.quickSortItems(result, 0, result.length - 1);
        return result;
    }

//    private void BFPRT()
//    {
//
//    }
//
//    private void bubbleSort(int length)
//    {
//        for(int i=0; i<length; i++)
//            bubbleSortIteration(length);
//    }
//
//    private void bubbleSortIteration(int length) // To low
//    {
//        if(!isNextNull() & length > 0)
//        {
//            if(next.val.getPrice()>val.getPrice())
//            {
//                MenuItem tmp = val;
//                val = next.val;
//                next.val = tmp;
//                length--;
//                next.bubbleSortIteration(length);
//            }
//        }
//    }
}
