package lab6.barBossHouse;

public class NegativeSizeException extends NegativeArraySizeException {
    public NegativeSizeException(String message) {
        super(message);
    }
}
