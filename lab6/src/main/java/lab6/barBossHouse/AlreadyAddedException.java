package lab6.barBossHouse;

public class AlreadyAddedException extends RuntimeException {
    public AlreadyAddedException(String message) {
        super(message);
    }
}
