package lab6.barBossHouse;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class OrderListIterator implements ListIterator<MenuItem> {
    private int index;
    private MenuItem[] items;
    private Order order;

    public OrderListIterator(Order order) {
        this(order, -1);//default index
    }

    public OrderListIterator(Order order, int index) {
        this.order = order;
        items = order.getItems();
        if (index < -1 | index > (items.length - 1)) throw new IllegalArgumentException();
        this.index = index;
    }

    @Override
    public boolean hasNext() {
        if (items.length - index > 1) {
            return (items[index + 1] != null);
        } else return false;
    }

    @Override
    public MenuItem next() {
        if (hasNext()) {
            index++;
            return items[index];
        } else throw new NoSuchElementException();
    }

    @Override
    public boolean hasPrevious() {
        return (index > 0);
    }

    @Override
    public MenuItem previous() {
        if (index > 0) {
            index--;
            return items[index];
        } else throw new NoSuchElementException();
    }

    @Override
    public int nextIndex() {
        if (hasNext()) return index + 1;
        else return items.length;
    }

    @Override
    public int previousIndex() {
        if (hasPrevious()) return index;
        else return -1;
    }

    @Override
    public void remove() {
        if (index > 0 & items[index] != null) {
            order.remove(index);
            items = order.getItems();
        }
    }

    @Override
    public void set(MenuItem menuItem) {
        if (index > 0) {
            items[index] = menuItem;
        }

    }

    @Override
    public void add(MenuItem menuItem) {
        if (order.add(menuItem)) {
            items = order.getItems();
        }
    }
}
