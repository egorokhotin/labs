package lab3.barBossHouse;

public class TableOrder implements Order {
    private static final byte DEFAULT_ORDER_LENGTH = 50;
    private static final Customer DEFAULT_CUSTOMER_ORDER_VALUE = new Customer();
    private MenuItem[] menuItems;
    private byte size;
    private Customer customer;

    public TableOrder() {
        this(Util.DEFAULT_ORDER_LENGTH, Util.DEFAULT_CUSTOMER_ORDER_VALUE);
    }

    public TableOrder(byte dishCount, Customer customer) {
        this(new MenuItem[dishCount], customer);
    }

    public TableOrder(MenuItem[] dishesArray, Customer customer) {
        menuItems = dishesArray;
        size = getSize();
        this.customer = customer;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean addItem(MenuItem menuItem) {
        if (size < menuItems.length) {
            menuItems[size] = menuItem;
            size++;
            return true;

        } else {
            MenuItem[] newMenuItem = new MenuItem[size * 2];
            System.arraycopy(menuItems, 0, newMenuItem, 0, size);
            menuItems = newMenuItem;
            menuItems[size] = menuItem;
            size++;
            return true;
        }
    }

    public boolean removeItem(String itemName) {
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], itemName)) {
                System.arraycopy(menuItems, i + 1, menuItems, i, size - i);
//                for (int j = i; j < size; j++) {
//                    menuItems[j] = menuItems[j + 1];
//                }
                size--;
            }
        }
        return true;
    }

    public boolean removeItem(MenuItem item) {
        for (int i = 0; i < size; i++) {
            if (menuItems[i].equals(item)) {
                System.arraycopy(menuItems, i + 1, menuItems, i, size - i);
//                for (int j = i; j < size; j++) {
//                    menuItems[j] = menuItems[j + 1];
//                }
                size--;
            }
        }
        return true;
    }

    public int removeAllItems(String itemName) {
        int deletedDishCount = 0, j = 0;
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], itemName)) {
                menuItems[i] = null;
                deletedDishCount++;
            }
        }
        for (int i = 0; i < size - 1; i++) {
            if (menuItems[i] == null) {
                menuItems[i] = menuItems[i + 1];
                menuItems[i + 1] = null;
            }
        }
        size -= deletedDishCount;
        return deletedDishCount;
    }

    public int removeAllItems(MenuItem item) {
        int deletedDishCount = 0, j = 0;
        for (int i = 0; i < size; i++) {
            if (menuItems[i].equals(item)) {
                menuItems[i] = null;
                deletedDishCount++;
            }
        }
        for (int i = 0; i < size - 1; i++) {
            if (menuItems[i] == null) {
                j = i;
                while ((menuItems[j] == null) & j < size) {
                    Util.shiftArray(menuItems, j);
                    j++;
                }
            }
        }
        size -= deletedDishCount;
        return deletedDishCount;
    }

    public int getItemsCount() {
        return size;
    }

    public MenuItem[] getItems() {
        MenuItem[] result = new MenuItem[size];
        System.arraycopy(menuItems, 0, result, 0, size);
        return result;
    }

    public double getSummaryCost() {
        double result = 0;
        for (int i = 0; i < size; i++) {
            result += menuItems[i].getPrice();
        }
        return result;
    }

    public int getItemSummaryCost(String name) {
        int result = 0;
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], name)) result += menuItems[i].getPrice();
        }
        return result;
    }

    public String[] getItemsNames() {
        if (size != 0) {
            String[] buffer = new String[size];
            boolean isAdded = false;
            int indx = 0;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < indx; j++) {
                    if (isEqualsNames(menuItems[i], buffer[j])) {
                        isAdded = true;
                        break;
                    }
                }
                if (isAdded) {
                    isAdded = false;
                    continue;
                } else {
                    buffer[indx] = menuItems[i].getName();
                    indx++;
                }
            }
            String[] names = new String[indx];
            System.arraycopy(names, 0, buffer, 0, buffer.length);
            return names;
        }
        return null;
    }

    public MenuItem[] getItemsSortedToLow() {
        if (size != 0) {
            MenuItem[] result = new MenuItem[size];
            System.arraycopy(menuItems, 0, result, 0, size);
            Util.quickSortItems(result, 0, size - 1);
            return result;
        }
        return null;
    }

    @Override
    public String toString() {
        String tmpString = "TableOrder: " + size + "\n";
        for (int i = 0; i < size; i++) {
            tmpString += menuItems[i].toString();
        }
        return tmpString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof TableOrder) {
            TableOrder order = (TableOrder) o;
            return customer.equals(order.customer) & equalsMenues(order);

        } else return false;
    }

    @Override
    public int hashCode() {
        int hash = customer.hashCode();
        for (int i = 0; i < size; i++)
            hash ^= menuItems[i].hashCode();
        return hash;
    }

    public int getItemsByType(MenuItem item) {
        int result = 0;
        if (item == null) {
            return 0;
        }

        String className = item.getClass().getName();
        for (int i = 0; i < size; i++) {
            if (isEqualsTypes(menuItems[i], className))
                result++;
        }
        return result;
    }

    public int getItemCount(String name) {
        int result = 0;
        for (int i = 0; i < size; i++) {
            if (isEqualsNames(menuItems[i], name)) {
                result++;
            }
        }
        return result;
    }

    private byte getSize() {
        for (byte i = 0; i < menuItems.length; i++) {
            if (menuItems[i] == null) return i;
        }
        return -1;
    }

    private boolean equalsMenues(TableOrder order) {
        int addFlag = 0;
        MenuItem[] tmpArr1 = new MenuItem[size];
        MenuItem[] tmpArr2 = new MenuItem[order.size];
        System.arraycopy(tmpArr1, 0, menuItems, 0, size);
        System.arraycopy(tmpArr2, 0, order.menuItems, 0, size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < order.size; j++) {
                if (tmpArr2[j] != null) {
                    if (tmpArr1[i].equals(tmpArr2[j])) {
                        addFlag = 1;
                        tmpArr1[i] = tmpArr2[j] = null;
                        break;
                    }
                }
            }
            if (addFlag == 0) {
                return false;
            } else {
                addFlag = 0;
            }
        }
        return true;
    }

    private boolean isEqualsTypes(MenuItem item, String className) {
        return (className.equalsIgnoreCase(item.getClass().getName()));
    }

    private boolean isEqualsNames(MenuItem item, String name) {
        return (name.equalsIgnoreCase(item.getName()));
    }


}
