package lab3.barBossHouse;

import java.util.function.Predicate;

public class TableOrdersManager implements OrdersManager {
    private static final int DEFAULT_CAPACITY_VALUE = 0;
    private TableOrder[] tableOrders;
    private int capacity;

    public TableOrdersManager(int tablesCount) {
        tableOrders = new TableOrder[tablesCount];
        capacity = DEFAULT_CAPACITY_VALUE;
    }

    public void addOrder(int tableNumber, TableOrder newTableOrder) {
        if (newTableOrder != null & isValidNumber(tableNumber)) {
            tableOrders[tableNumber] = newTableOrder;
            capacity++;
        }
    }

    public TableOrder getOrder(int tableNumber) {
        if (isValidNumber(tableNumber)) {
            return tableOrders[tableNumber];
        } else return null;
    }

    public boolean addItemToOrder(int tableNumber, MenuItem menuItem) {
        if (menuItem != null) {
            if (isValidNumber(tableNumber)) {
                tableOrders[tableNumber].addItem(menuItem);
                return true;
            }
        }
        return false;
    }

//    public int getCommonItems(MenuItem item)
//    {
//        int count = 0;
//        int[] ordersIndexes = getBusyTables();
//        for(int i=1; i<ordersIndexes.length; i++)
//        {
//            count += tableOrders[ordersIndexes[i]].getItemsByType(item);
//        }
//
//        return count;
//    }

    public int removeOrder(TableOrder order) {
        int[] ordersIndexes = getBusyTables();
        for (int i = 0; i < ordersIndexes.length; i++) {
            if (tableOrders[ordersIndexes[i]].equals(order)) {
                tableOrders[ordersIndexes[i]] = null;
                capacity--;
                return ordersIndexes[i];
            }
        }
        return -1;
    }

    public int removeAllOrders(TableOrder order) {
        int[] ordersIndexes = getBusyTables();
        int count = 0;
        for (int i = 0; i < ordersIndexes.length; i++) {
            if (tableOrders[ordersIndexes[i]].equals(order)) {
                tableOrders[ordersIndexes[i]] = null;
                capacity--;
                count++;
            }
        }
        if (count != 0) return count;
        else return -1;
    }

    public void freeTable(int tableNumber) {
        if (isValidNumber(tableNumber)) {
            tableOrders[tableNumber] = null;
            capacity--;
        }
    }

    public int getFirstFreeTableNumber() {
        for (int i = 0; i < tableOrders.length; i++) {
            if (tableOrders[i] == null) return i;
        }

        return (-1);
    }

    public int[] getFreeTables() {
        return getTables(x -> x != null);
    }

    public int[] getBusyTables() {
        return getTables(x -> x == null);
    }

    public TableOrder[] getOrders() {
        TableOrder[] result = null;
        if (capacity != 0) {
            result = new TableOrder[capacity];
            int j = 0;
            for (int i = 0; i < tableOrders.length; i++) {
                if (tableOrders[i] != null) {
                    result[j] = tableOrders[i];
                    j++;
                }
            }
            return result;
        }
        return result;
    }

    public int getSummaryItemCount(String name) {
        int[] freeIndxs = getBusyTables();
        int result = 0;
        for (int i = 0; i < freeIndxs.length; i++) {
            result += tableOrders[freeIndxs[i]].getItemCount(name);
        }
        return result;
    }

    public int getSummaryItemCountByType(MenuItem item) {
        int[] freeIndxs = getBusyTables();
        int result = 0;
        for (int i = 0; i < freeIndxs.length; i++) {
            result += tableOrders[freeIndxs[i]].getItemsByType(item);
        }
        return result;
    }

    private int[] getTables(Predicate<Order> predicate) {
        int[] result = null;
        if (capacity != 0) {
            result = new int[capacity];
            int j = 0;
            for (int i = 0; i < tableOrders.length; i++) {
                if (predicate.test(tableOrders[i])) {
                    result[j] = i;
                    j++;
                }
            }
        }
        return result;
    }

    public double getSummaryCost() {
        double sum = 0;
        TableOrder[] ordersTempArray = getOrders();
        if (ordersTempArray != null) {
            for (int i = 0; i < ordersTempArray.length; i++) {
                sum += ordersTempArray[i].getSummaryCost();
            }
        }
        return sum;
    }

    public int getOrdersCount() {
        return capacity;
    }

    private boolean isValidNumber(int number) {
        if (number > -1 & number < tableOrders.length) {
            return true;
        } else {
            return false;
        }
    }
}
