package lab3.barBossHouse;

public class MenuItem {
    protected final static int DEFAULT_PRICE = 0;
    protected final static String DEFAULT_STRING = "";
    private String name;
    private int price;
    private String description;

    protected MenuItem(String dishName, String dishDescription) {
        this(dishName, dishDescription, DEFAULT_PRICE);
    }

    protected MenuItem(String dishName, String dishDescription, double dishPrise) {
        name = dishName;
        description = dishDescription;
        price = transferInCents(dishPrise);
    }

    public double getPrice() {
        return transferInRub(price);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return getNameString() +
                getPriceString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof MenuItem) {
            MenuItem item = (MenuItem) o;
            return (price == item.getPrice()) &
                    name.equalsIgnoreCase(item.getName());

        } else return false;
    }

    @Override
    public int hashCode() {
        return price ^ name.hashCode() ^ description.hashCode();
    }

    private String getNameString() {
        if (name.equalsIgnoreCase(DEFAULT_STRING)) {
            return "";
        } else return name + ", ";
    }

    private String getPriceString() {
        if (price == DEFAULT_PRICE) {
            return "";
        } else return String.valueOf(price) + "p.";
    }

    private int transferInCents(double val) {
        return (int) (val * 100);
    }

    private double transferInRub(int val) {
        return ((double) val) / 100.0;
    }

}
