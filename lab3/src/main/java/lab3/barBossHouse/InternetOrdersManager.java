package lab3.barBossHouse;

public class InternetOrdersManager implements OrdersManager {
    private InternetOrder order;
    private int count;

    public InternetOrdersManager() {
        this(new InternetOrder[Util.DEFAULT_LENGTH]);
    }

    public InternetOrdersManager(InternetOrder[] orders) {
        parseOrdersArray(orders);
        this.count = Util.DEFAULT_COUNT_VALUE;
    }

    public boolean enqueOrder(InternetOrder order) {
        count++;
        boolean result = this.order.enqueue(order);
        return result;
    }

    public InternetOrder getFirst() {
        return order.getFirst();
    }

    public InternetOrder dequeue() {
        count--;
        return order.dequeue();
    }

    public int getOrdersCount() {
        return count;
    }

    public InternetOrder[] getOrders() {
        InternetOrder[] array = new InternetOrder[count];
        for (int i = Util.FIRST_QUEUE_POSITION, j = 0; j < array.length; j++, i++)
            array[j] = order.getOrder(i);
        return array;
    }

    public double getSummaryCost() {
        InternetOrder[] tmp = getOrders();
        double result = 0;
        if (tmp != null) {
            for (int i = 0; i < tmp.length; i++)
                result += tmp[i].getSummaryCost();
        }
        return result;
    }

    public int getSummaryItemCount(String itemName) {
        InternetOrder[] tmp = getOrders();
        int result = 0;
        if (tmp != null) {
            for (int i = 0; i < tmp.length; i++) {
                result += tmp[i].getItemCount(itemName);
            }
        }
        return result;
    }

    public int getSummaryItemCountByType(MenuItem item) {
        InternetOrder[] tmp = getOrders();
        int result = 0;
        if (tmp != null) {
            for (int i = 0; i < tmp.length; i++) {
                result += tmp[i].getItemsByType(item);
            }
        }
        return result;
    }

    private void parseOrdersArray(InternetOrder[] orders) {
        order = new InternetOrder();
        for (int i = 0; i < orders.length; i++) {
            count++;
            order.enqueue(orders[i]);
        }
    }


}
