package lab3.barBossHouse;

public class InternetOrder implements Order {
    private static final Customer DEFAULT_CUSTOMER_VALUE = null;
    private static final int DEFAULT_LENGTH = 0;
    private InternetOrder nextOrder;
    private Customer customer;
    private int itemsCount;
    private ItemsList list;

    public InternetOrder() {
        this(Util.DEFAULT_CUSTOMER_VALUE, new MenuItem[Util.DEFAULT_LENGTH]);
    }

    public InternetOrder(Customer customer, MenuItem[] menuItems) {
        this.customer = customer;
        this.itemsCount = menuItems.length;
        //this.list = Util.DEFAUL_ITEMSLIST_VALUE;
        addItems(menuItems);
    }

    public boolean enqueue(InternetOrder order) {
        if (nextOrder == null) {
            this.nextOrder = order;
            return true;
        } else {
            return nextOrder.enqueue(order);
        }
    }

    public InternetOrder dequeue() {
        InternetOrder result = new InternetOrder();
        result.list = nextOrder.list;
        result.customer = nextOrder.customer;
        nextOrder.shiftQueue();
        return result;
    }

    public InternetOrder getFirst() {
        InternetOrder result = new InternetOrder();
        result.list = nextOrder.list;
        result.customer = nextOrder.customer;
        return result;
    }

    public InternetOrder getOrder(int indx) {
        if (nextOrder != null) {
            if (indx > 1) {
                return nextOrder.getOrder(--indx);
            } else return nextOrder;
        } else return null;
    }

    public boolean addItem(MenuItem item) {
        return list.add(item);
    }

    public boolean removeItem(MenuItem item) {
        return list.remove(item);
    }

    public boolean removeItem(String itemName) {
        return list.remove(itemName);
    }

    public int removeAllItems(String itemName) {
        return list.removeAll(itemName);
    }

    public int removeAllItems(MenuItem item) {
        return list.removeAll(item);
    }

    @Override
    public int getItemsCount() {
        return list.getItemsCount();
    }

    public MenuItem[] getItems() {
        return list.getItems();
    }

    public double getSummaryCost() {
        return list.getSummaryCost();
    }

    public int getItemCount(String itemName) {
        return list.getItemCount(itemName);
    }

    public int getItemsByType(MenuItem item) {
        String typeName = item.getClass().getName();
        return list.getItemsCountByType(typeName);
    }

    public String[] getItemsNames() {
        return list.getItemsNames();
    }

    public MenuItem[] getItemsSortedToLow() {
        return list.getItemsSortedToLow();
    }

    public void setCustomer(Customer customer) {
        customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        ;
        String tmp = "InternetOrder: " + customer.toString() + "\n" + list.getItemsCount();
        tmp += list.toString();
        return tmp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof InternetOrder) {
            InternetOrder order = (InternetOrder) o;
            MenuItem[] items = getItems();
            MenuItem[] orderItems = order.getItems();
            if (customer.equals(order.customer) & itemsCount == order.itemsCount) {
                boolean isAdded = false;
                for (int i = 0; i < itemsCount; i++) {
                    for (int j = 0; j < itemsCount; j++) {
                        if (items[i].equals(orderItems[j])) {
                            isAdded = true;
                        }
                    }
                    if (!isAdded) {
                        return false;
                    } else {
                        isAdded = false;
                    }
                }
                return true;
            } else return false;
        } else return false;
    }

    @Override
    public int hashCode() {
        return itemsCount ^ customer.hashCode() ^ list.hashCode();
    }

    private void addElement(InternetOrder order) {
        if (nextOrder != null) {
            nextOrder.addElement(order);
        } else nextOrder = order;
    }

    private void removeThis() {
        list = null;
        customer = null;
        itemsCount = 0;
    }

    private void shiftQueue() {
        if (nextOrder != null) {
            list = nextOrder.list;
            customer = nextOrder.customer;
            nextOrder = nextOrder.nextOrder;
        }
    }

    private void addItems(MenuItem[] items) {
        if (items.length > 0) {
            list = new ItemsList(null);
            for (int i = 0; i < items.length; i++) {
                list.add(items[i]);
            }
        } else list = new ItemsList(null);

    }
}
