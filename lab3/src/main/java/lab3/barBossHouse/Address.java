package lab3.barBossHouse;

public class Address {
    private final String cityName;
    private final int zipCode;
    private final String streetName;
    private final int buildingNumber;
    private final char buildingLetter;
    private final int apartmentNumber;

    public Address() {
        this(Util.DEFAULT_STRING, Util.DEFAULT_INT, Util.DEFAULT_STRING, Util.DEFAULT_INT, Util.DEFAULT_CHAR, Util.DEFAULT_INT);
    }

    public Address(String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        this(Util.DEFAULT_CITY, Util.DEFAULT_INT, streetName, buildingNumber, buildingLetter, apartmentNumber);
    }

    public Address(String cityName, int zipCode, String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        this.cityName = cityName;
        this.zipCode = zipCode;
        this.streetName = streetName;
        this.buildingNumber = buildingNumber;
        this.buildingLetter = buildingLetter;
        this.apartmentNumber = apartmentNumber;
    }

    public String getCityName() {
        return cityName;
    }

    public int getZipCode() {
        return zipCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public char getBuildingLetter() {
        return buildingLetter;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }

    //REFACTOR
    @Override
    public String toString() {
        return "Address: " +
                getStringValue(getCityName()) +
                getZipCodeString() +
                getStringValue(getStreetName()) +
                getStringValue(getBuildingNumber()) +
                getLiteralString() +
                getStringValue(getApartmentNumber());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof Address) {
            Address address = (Address) o;
            return cityName.equalsIgnoreCase(address.getCityName()) &
                    (zipCode == address.getZipCode()) &
                    (streetName.equalsIgnoreCase(address.getStreetName())) &
                    (buildingNumber == address.getBuildingNumber()) &
                    (buildingLetter == address.getBuildingLetter()) &
                    (apartmentNumber == address.getApartmentNumber());
        } else return false;
    }

    @Override
    public int hashCode() {
        return cityName.hashCode() ^
                zipCode ^
                streetName.hashCode() ^
                buildingNumber ^
                buildingLetter ^
                apartmentNumber;
    }

    private String getZipCodeString() {
        if (zipCode == Util.DEFAULT_INT) {
            return ", ";
        } else return " " + String.valueOf(zipCode) + ", ";
    }

    private String getLiteralString() {
        if (buildingLetter == Util.DEFAULT_CHAR) {
            //if(bu)
            return String.valueOf(buildingLetter);
        } else return " " + buildingLetter;
    }

    private String getApartmentNumberString() {
        if (apartmentNumber == Util.DEFAULT_INT) {
            return "";
        } else return "-" + String.valueOf(apartmentNumber);
    }

    private String getStringValue(int val) {
        if (val != Util.DEFAULT_INT) return String.valueOf(val) + " ";
        else return "";
    }

    private String getStringValue(String val) {
        if (val.equalsIgnoreCase(Util.DEFAULT_STRING)) return String.valueOf(val) + " ";
        else return "";
    }
}
