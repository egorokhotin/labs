package lab3.barBossHouse;

public final class Util {
    private Util() {

    }

    //public enum ItemType {Drink, Dish, MenuItem, NaN}

    public static final int DEFAULT_INT = -1;
    public static final String DEFAULT_STRING = "";
    //public static final int DEFAULT_PRICE = 0;
    public static final int DEFAULT_ALCOHOL_PERCENT = 0;
    public static final int DEFAULT_COUNT_VALUE = 0;
    //public static final int DEFAULT_QUEUE_LENGTH = 0;
    public static final int DEFAULT_LENGTH = 0;
    public static final int FIRST_QUEUE_POSITION = 1;
    public static final Customer DEFAULT_CUSTOMER_VALUE = null;
    public static final String DEFAULT_CITY = "Samara";
    public static final char DEFAULT_CHAR = ' ';
    //public static final MenuItem DEFAULT_ITEM_VALUE = null;
    //public static final ItemsList DEFAUL_ITEMSLIST_VALUE = new ItemsList(null);

    public static final byte DEFAULT_ORDER_LENGTH = 50;
    public static final Customer DEFAULT_CUSTOMER_ORDER_VALUE = new Customer();

    public static void quickSortItems(MenuItem[] array, int lowIndx, int highIndx) {
        MenuItem menuItem = array[(lowIndx + highIndx) / 2];
        double menuItemPrice = menuItem.getPrice();

        while (lowIndx <= highIndx) {
            while (array[lowIndx].getPrice() > menuItemPrice) lowIndx++;
            while (array[highIndx].getPrice() < menuItemPrice) highIndx--;
            if (lowIndx <= highIndx) {
                MenuItem tmpItem = array[highIndx];
                array[highIndx] = array[lowIndx];
                array[lowIndx] = tmpItem;
                lowIndx++;
                highIndx--;
            }
            if (lowIndx < highIndx) quickSortItems(array, lowIndx, highIndx);
            if (highIndx > lowIndx) quickSortItems(array, lowIndx, highIndx);
        }
    }

    public static <E> void shiftArray(E[] array, int indx) {
        for (int i = indx; i < array.length + 1; i++)
            array[i] = array[i + 1];
    }

    public static <E> int getNotNullCount(E[] array) {
        int result = 0;
        for (int i = 0; i < array.length; i++)
            if (array[i] != null) result++;
        return result;
    }
}
