package lab3.barBossHouse;

public class Dish extends MenuItem {

    public Dish(String name, String description) {
        this(name, description, DEFAULT_PRICE);
    }

    public Dish(String name, String description, double price) {
        super(name, description, price);
    }


    @Override
    public String toString() {
        return "Dish: " +
                super.toString() +
                getDescriptionString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof Dish) {
            //Dish dish = (Dish)o;
            return super.equals(o);
            //return getName().equalsIgnoreCase(dish.getName()) &
            //(getPrice() == dish.getPrice());
        } else return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    private String getDescriptionString() {
        if (super.getDescription().equalsIgnoreCase(DEFAULT_STRING)) {
            return "";
        } else return super.getDescription();
    }
}
