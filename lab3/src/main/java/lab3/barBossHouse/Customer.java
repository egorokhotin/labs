package lab3.barBossHouse;

public class Customer {
    private final String firstName;
    private final String secondName;
    private final int age;
    private final Address address;

    public static final Customer MATURE_UNKNOWN_CUSTOMER = new Customer(21);
    public static final Customer NOT_MATURE_UNKNOWN_CUSTOMER = new Customer(14);

    public Customer() {
        this(Util.DEFAULT_INT);
    }

    public Customer(int age) {
        this(Util.DEFAULT_STRING, Util.DEFAULT_STRING, age, new Address());
    }

    public Customer(String firstName, String secondName, int age, Address address) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return age;
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Customer: " +
                getStringName() +
                getStringAge() +
                address.toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof Customer) {
            Customer customer = (Customer) o;
            return firstName.equalsIgnoreCase(customer.getFirstName()) &
                    secondName.equalsIgnoreCase((customer.getSecondName())) &
                    (age == customer.getAge()) &
                    address.equals(customer.getAddress());
        } else return false;
    }

    @Override
    public int hashCode() {
        return age ^ firstName.hashCode() ^
                secondName.hashCode() ^
                address.hashCode();
    }

    private String getStringName() {
        String result = "";
        result += parseValue(secondName);
        result += parseValue(firstName);
        result += ", ";
        return result;
    }

    private String getStringAge() {
        String result = "";
        if (parseValue(age).equalsIgnoreCase("")) {
            return "";
        } else return parseValue(age) + ", ";
    }

    private String parseValue(String s) {
        if (s.equalsIgnoreCase(Util.DEFAULT_STRING)) return "";
        else return s + " ";
    }

    private String parseValue(int num) {
        if (num == Util.DEFAULT_INT) return "";
        else return String.valueOf(num);
    }

}
