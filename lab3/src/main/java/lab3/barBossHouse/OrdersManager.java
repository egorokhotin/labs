package lab3.barBossHouse;

public interface OrdersManager {
    public int getOrdersCount();

    public Order[] getOrders();

    public double getSummaryCost();

    public int getSummaryItemCount(String itemName);

    public int getSummaryItemCountByType(MenuItem item);
}

