package lab3;


public class Main {

    public static void main(String[] args) {
        TestClass tClass = new TestClass();
        System.out.println(tClass.tableOrdersManager_addOrder_ReturnTrue());
        System.out.println(tClass.tableOrdersManager_removeOrder_ReturnTrue());
        System.out.println(tClass.tableOrdersManager_removeAllOrders_ReturnTrue());
        System.out.println(tClass.tableOrdersManager_getSummaryCost_ReturnTrue());
        System.out.println(tClass.tableOrdersManager_getSummaryItemCount_ByType());
        System.out.println(tClass.tableOrdersManager_getItemCount_ByName());

        System.out.println(tClass.internetOrdersManager_enqueueOrder());
        System.out.println(tClass.internetOrdersManager_dequeueOrder());
        System.out.println(tClass.internetOrdersManager_getSummaryItemCount());
        System.out.println(tClass.internetOrdersManager_getSummaryItemCount_ByType());
    }

}
